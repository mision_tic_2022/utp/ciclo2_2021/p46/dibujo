import javax.swing.JPanel;
import java.awt.event.*;
import java.awt.*;

public class PanelDibujo extends JPanel {
    /**************
     * Atibutos
     ***********/
    private int contar_puntos;
    private Point[] puntos;

    /****************
     * Constructor
     ***************/
    public PanelDibujo(){
        //Inicializar los atributos
        this.contar_puntos = 0;
        this.puntos = new Point[10000];

        //Poner a la escucha de eventos de mouse
        this.addMouseMotionListener( new MouseMotionAdapter(){
            //Captura los eventos del mouse en moviemiento con click sostenido
            public void mouseDragged(MouseEvent evt){
                if(contar_puntos < puntos.length){
                    //Obtenemos la coordenada(punto) del click
                    puntos[contar_puntos] = evt.getPoint();
                    contar_puntos++;
                    repaint();
                }
            }
        });
    }//Fin del constructor

    public void paintComponent(Graphics graphics){
        //Borrar el área de dibujo
        super.paintComponent(graphics);
        //Dibujar los puntos en el arreglo
        for(int i = 0; i < contar_puntos; i++){
            graphics.fillOval(puntos[i].x, puntos[i].y, 4, 4);
        }
    }


}