import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.*;

public class App {
    public static void main(String[] args) throws Exception {
        crear_lienzo();
    }

    public static void crear_lienzo(){
        //crear la ventana
        JFrame objFrame = new JFrame();
        //Indicar el titulo de la ventana
        objFrame.setTitle("Programa para dibujar");
        //Construir objeto de tipo PanelDibujo
        PanelDibujo objPanelDibujo = new PanelDibujo();
        //Agregar elementos a la ventana
        objFrame.add(objPanelDibujo, BorderLayout.CENTER);
        //Etiqueta que representa un titulo
        JLabel titulo = new JLabel("Click sostenido para dibujar");
        objFrame.add(titulo, BorderLayout.NORTH);

        /******************************
         * Configuración de la ventana
         ******************************/
        objFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        objFrame.setSize(400, 200);
        objFrame.setVisible(true);

    }
    
}
